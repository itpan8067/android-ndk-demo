#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring

JNICALL
Java_dy_video_android_1ndk_1demo_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}
